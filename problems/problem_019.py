# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

# def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
#     if x >= rect_x and y >= rect_y and x <= (rect_x + rect_width) and y <= (rect_y + rect_height):
#         return True
#     else:
#         return False

# print(is_inside_bounds(15,15,10,10,15,15))

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    check_list = []
    if x >= rect_x:
        check_list.append(True)
    else:
        check_list.append(False)

    if y >= rect_y:
        check_list.append(True)
    else:
        check_list.append(False)

    if x <= (rect_x + rect_width):
        check_list.append(True)
    else:
        check_list.append(False)

    if y <= (rect_y + rect_height):
        check_list.append(True)
    else:
        check_list.append(False)

    print(check_list)
    return all(check_list)


print(is_inside_bounds(55,55,10,10,15,15))
