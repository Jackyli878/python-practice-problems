# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
number_list = [10,20,30,70,50]
def find_second_largest(number_list):
        if len(number_list) <=1:
             return None
        else:
            number_list.remove(max(number_list))
            return (max(number_list))

print(find_second_largest(number_list))
