# Write a function that meets these requirements.
#
# Name:       num_concat
# Parameters: two numerical parameters
# Returns:    the concatenated string conversions
#             of the numerical parameters
#
# Examples:
#     input:
#       parameter 1: 3
#       parameter 2: 10
#     returns: "310"
#     input:
#       parameter 1: 9238
#       parameter 2: 0
#     returns: "92380"

parameter_1 = 92380
parameter_2 = 0

def num_concat(parameter_1, parameter_2):
    parameter_1 = str(parameter_1)
    parameter_2 = str(parameter_2)
    return parameter_1 + parameter_2

print(num_concat(parameter_1, parameter_2))
