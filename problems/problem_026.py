# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    grades = []
    for element in values:
        if element >= 90:
            grade = "A"
        elif element >= 80 and element < 90:
            grade = "B"
        elif element >= 70 and element < 80:
            grade = "C"
        elif element >= 60 and element < 70:
            grade = "D"
        else:
            grade = "F"
        grades.append(grade)
    return grades


def grade_list():
    grade_list = []
    while True:
        grade = input("Please eneter score or 'done' to stop:")
        if grade == 'done':
            break
        else:
           grade_list.append(float(grade))
    return grade_list

values = grade_list()
grades = calculate_grade(values)
print(grades)
