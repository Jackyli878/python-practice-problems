# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
def inputs():
    age = int(input("What is your age?:"))
    consent = bool(input("Is your consent form signed?: (if no please leave this blank)"))
    if consent != True:
       consent = False
    return(age,consent)

def can_skydive(age,consent):
    if age >= 18 and consent == True:
      print("You are eligible for sky diving.")
    elif age < 18:
      print("Sorry you are too young.")
    elif consent == False:
      print("Sorry your consent form is not signed.")

input_values=inputs()
can_skydive(input_values[0],input_values[1])
