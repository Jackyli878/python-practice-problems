# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

input = 3

def sum_fraction_sequence(input):
    sum = 0
    for number in range(1, input + 1):
        sum += number / (number + 1)
    return sum

print(sum_fraction_sequence(input))
