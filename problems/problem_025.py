# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
def input_values():
    values = []
    while True:
        number = input("Please enter a number or enter 'done' to stop:")
        if number == 'done':
            break
        else:
            values.append(float(number))
    return values

def calculate_sum(values):
    if values:
        total = sum(values)
        return total
    if len(values) == 0:
        return None

values = input_values()
print(values)
print(calculate_sum(values))
