# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    if value1 > value2:
        print("The minimum of the two is ", value2)
        return value2
    elif value1 < value2:
        print("The minimum of the two is", value1)
        return value1
    else:
        print(value1, "and", value2, "are the same")
        return value1

def inputs():
    value1 = float(input("Please enter in a number:"))
    value2 = float(input("Please enter in another number:"))
    return (value1,value2)

input_values = inputs()
minimum_value(input_values[0],input_values[1])
