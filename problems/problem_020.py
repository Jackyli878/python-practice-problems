# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

members_list= ["Shari", "Gwen", "Jean",
                "Bryant","Emmett","Ray",
                "Ted", "Jack", "Tom",
                "Sue", "Joe", "Jill"]

attendees_list = ["Shari", "Gwen", "Jean",
                  "Bryant","Gerald",]

def has_quorum(attendees_list, members_list):
    number_of_members = len(members_list)
    number_of_attendees = len(attendees_list)
    minimum = (number_of_members/2)
    if number_of_attendees >= minimum:
        return True
    else:
        return False

def member_check(attendees_list, members_list):
    for attendee in attendees_list:
        if attendee not in members_list:
            return False
    return True

print(has_quorum(attendees_list, members_list))
print(member_check(attendees_list, members_list))
