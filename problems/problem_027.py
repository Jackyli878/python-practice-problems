# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#
values = []

def max_in_list(values):
    if len(values) == 0:
        return None
    else:
        max_number = max(values)
        return max_number

print(max_in_list(values))
