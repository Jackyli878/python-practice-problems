# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]
number = [1, 3, 3, 7]

def only_odds(number):
    return_list = []
    for index in number:
        if index % 2 != 0:
            return_list.append(index)
    return return_list

print(only_odds(number))
