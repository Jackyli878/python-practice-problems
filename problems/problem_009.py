# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# Use the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    given_word = word.lower()
    reversed_word = "".join(reversed(given_word))
    if reversed_word == given_word:
        print(word, "is a palindrome!")
    else:
        print(word, "is not a palindrome.")

word = input("Please give us a word:")

is_palindrome(word)
