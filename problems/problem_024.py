# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if values == True:
        total = sum(values)
        average = (total / len(values))
        return average
    if len(values) == 0:
        return None

def input_number():
    values = []
    while True:
        number = input("please input a number (or 'done' to stop): ")
        if number == 'done':
            break
        else:
            values.append(float(number))
    return values

values = input_number()
print(values,"sum",sum(values),"# of numbers",len(values))
print(calculate_average(values))
