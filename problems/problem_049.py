# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7
x = 1.3
y = 2.3
def sum_two_numbers(x,y):
    result = x +y
    return result
print(sum_two_numbers(x,y))
