# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}
dictionary = {"one": 1, "two": 2, "three": 3}
def reverse_dictionary(dictionary):
    new_dictionary = {}
    #assigns the value as the key and the key as the value from the dictionary by using the .items method to call the pair
    #dictionary[key] = new_value function is used here
    for every_key, every_value in dictionary.items():
        new_dictionary[every_value] = every_key
    return new_dictionary

print(reverse_dictionary(dictionary))
