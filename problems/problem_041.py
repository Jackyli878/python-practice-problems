# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

csv_lines = ["3", "1,9"]

def add_csv_lines(csv_lines):
    new_list = []
    for each_csv_string in csv_lines:
        new_csv = each_csv_string.split(",")
        csv_string_total = sum(int(element) for element in new_csv)
        new_list.append(csv_string_total)
    return new_list

print(add_csv_lines(csv_lines))
