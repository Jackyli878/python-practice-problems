# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
def inputs():
    value1 = float(input("Enter in a number:"))
    value2 = float(input("Enter in another number:"))
    value3 = float(input("Enter in one last number:"))
    value_list = [value1, value2, value3]
    return value_list

def max_of_three(value1, value2, value3):
   max_number = max(value1,value2,value3)
   return max_number ##can I return the max number instead or do I have to strictly return one of the 3 values

input_values = inputs()
output=max_of_three(input_values[0],input_values[1],input_values[2])
print("The max number is", output)
