# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x in range(0,11) and y in range (0,11):
        return True
    else:
        return False

x = int(input("Please give us coordinate x:"))
y = int(input("Please give us coordinate y:"))
print(is_inside_bounds(x,y))
