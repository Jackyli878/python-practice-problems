# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

list1 = [1, 2, 3, 4]
list2 = [4, 5, 6, 7]
def pairwise_add(list1, list2):
    result_list = []
    for element_1, element_2 in zip(list1, list2):
        result = element_1 + element_2
        result_list.append(result)
    return result_list

print(pairwise_add(list1, list2))
