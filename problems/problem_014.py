# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.



def can_make_pasta(ingredients):
    ingredients_list=["flour", "eggs", "oil"]
    if set(ingredients) == set(ingredients_list):
        print("Those are the ingredients for pasta")
        return True
    else:
        print("You cannot make pasta with those ingredients")
        return False

ingredients=[]
ingredients.append(input("Please selecet an ingredient for the list:"))
ingredients.append(input("Please selecet an ingredient for the list:"))
ingredients.append(input("Please selecet an ingredient for the list:"))
print(can_make_pasta(ingredients))
