# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

password = "123abcABC$!@"

def check_password(password):
    lowercase = False
    uppercase = False
    digit = False
    special = False
    length = False
    if len(password) >= 6 and len(password) <=12:
        length = True
    for string in password:
        if string.isalpha():
            if string.islower():
                lowercase = True
            elif string.isupper():
                uppercase = True
        elif string.isdigit():
            digit = True
    for string in password:
        if string in "$@!":
            special = True
            break
    return (
        lowercase
        and uppercase
        and digit
        and special
        and length
    )

print(check_password(password))
