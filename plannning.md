# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value
instructions: make a function that looks at 2 numbers and returns the lowest of the 2, if the numbers are the same return either or.

Psuedo code (use functions)
"minimum_value" function(parameter1[first number], parameter2[second number])
if first number is > second number
    print second number
if second number is > first number
    print first number
else
    print first number

call minimum value function function (test parameter 1, test parameter 2)

test case: negatives, zeros, floats, str
    all test cases passed, tweaked the code to add 2 input prompts as a function that request for user to enter in 2 numbers to compare


### 04 max_of_three
instructions: make a function that find the max of 3 numbers, if 2 number are the same return either of them, if all are the same return any of them

Psuedo code
"inputs" function that collects the 3 numbers from user
    first input = float (input)
    second_input = float (input)
    third_input = float(input)
    input list = [first_input, second_input, third_input]
    return input list

"max_of_three" function (1st number, 2nd number, 3rd number)
use max function to determine the max
variable = max number
if loop to determine if the max number is the same as 2nd and 3rd number
    else if loop to determine if the max number is the same as 2nd or 3rd number
    print that it is the same as the second or 3rd number
print that it is the same as both 2nd and 3rd number

call input list function
call max of three function

### 06 can_skydive
instructions: complete function to output if someone can go skydiving, person must be 18 or older and have a consent form signed.

Psuedo code:
input functions
age = int(input)
consent =(bool(input))

can sky dive function
if the person is 18 or older and their consent is true
    they are eligible
if they are younger than 18
    they are too young
if the consent is false
    they do not have the consent form signed

inputs = input function
can sky dive function

### 09 is_palindrome
instructions: check if word in parameter is the same forward and backward. Use the built in function reverse and join method for string object

Psuedo code:
Palindrome function
convert word to all lower case so that the string can reverse without na issue
variable to store reverse word
if reverse word  = word
    it is a palindrome
else
    it is no a plaindrome
word= input
call palindrom function

### 010 is_divisible_by_3
instructions: create a fucntion to return fizz if value is divisble by 3, if not return the number.

divisible by 3 function
if number that is divided by 3 has no remainder
    return fizz
else
    return number
number = input
call divisble by 3 function

### 011 is_divisible_by_5
instructions: create a function to return buzz if value is divisible by 5, if not return the number.

divisible by 5 function
if number that is divided by 5 has no reaminder
    return buzz
else
    return number
number = input
call divisible by 5 function

### 012 Fizz_buzz

### 016 is_inside_bounds
insturctions: takes x and y coordinate and makes sure they are between 0 and 10 inclusively

Psuedo code:
is_inside_bounds function
if x in range of (0,11) and y in range of (0,11)
    return true
else
    return false
x = int(input)
y = int(input)
print(is_inside_bounds(x,y))

### 019 is_inside_bounds
### 020 has_quoram
### 022 gear_for_day
### 024 calculate_average
### 025 calculate_sum
### 026 calculate_grade
### 027 max_in_list
### 028 remove_duplicate_letters
### 030 find_second_largets
### 031 sum_of_squares
### 032 sum_of_first_n_numbers
### 033 sum_of_first_n_even
### 034 count_letters_and_digits
### 037 pad_letf
### 039 reverse_dictionary
